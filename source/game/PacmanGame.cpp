#include "pch.h"

#include "PacmanGame.h"

#include "FrameRenderGraph.h"
#include "FrameUpdateGraph.h"
#include "engine/Application.h"
#include "engine/ResourceManager.h"
#include "engine/Sprite.h"
#include "engine/ecs/components/CameraComponent.h"
#include "engine/ecs/components/SpriteComponent.h"
#include "engine/ecs/components/TransformComponent.h"
#include "engine/ecs/systems/CameraSystem.h"
#include "game/ecs/systems/SceneSerializerSystem.h"

PacmanGame::PacmanGame() = default;
PacmanGame::~PacmanGame() = default;

void PacmanGame::OnUpdate(float deltaTime)
{
    m_updateGraph->Update(deltaTime);
}

void PacmanGame::OnRender(float deltaTime)
{
    m_renderGraph->Draw(deltaTime);
}

void PacmanGame::OnStart()
{
    using namespace engine::ecs;

    auto& world = engine::Application::Instance().GetWorld();

    // Scene Serializer
    {
        m_serializer = world.RegisterSystem<SceneSerializerSystem>();
        world.SetSystemComponentMask<SceneSerializerSystem>({});
    }

    // Other ECS game-specific systems

    TestLoad();
}

void PacmanGame::OnFinish()
{
    m_serializer->Save(R"(../../resources/scenes/test.scene)");
}

void PacmanGame::TestLoad()
{
    m_serializer->Load(R"(../../resources/scenes/test.scene)");
    auto& world = engine::Application::Instance().GetWorld();
    auto cameraSystem = world.GetSystem<engine::ecs::CameraSystem>();
    cameraSystem->SetActiveCamera(0);
}
