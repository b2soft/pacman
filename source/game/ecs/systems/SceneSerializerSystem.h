#pragma once

#include "engine/ecs/ECS.h"

class SceneSerializerSystem : public engine::ecs::System
{
public:
    void Load(std::string_view name);
    void Save(std::string_view name);
};