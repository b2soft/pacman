#include "pch.h"

#include "SceneSerializerSystem.h"

#include <nlohmann/json.hpp>
#include "engine/Application.h"
#include "engine/Errors.h"
#include "engine/ResourceManager.h"
#include "engine/ecs/components/CameraComponent.h"
#include "engine/ecs/components/DoNotSerializeComponent.h"
#include "engine/ecs/components/NameComponent.h"
#include "engine/ecs/components/SpriteComponent.h"
#include "engine/ecs/components/TransformComponent.h"
#include "engine/render/Shader.h"

void SceneSerializerSystem::Load(std::string_view name)
{
    namespace fs = std::filesystem;
    const fs::path filePath(name);

    if (!fs::exists(filePath))
    {
        engine::FatalError("Scene file not found");
    }

    using json = nlohmann::json;
    std::ifstream ifs(name.data());
    json j = json::parse(ifs);

    auto entities = j["entities"];

    using namespace engine::ecs;
    auto& world = engine::Application::Instance().GetWorld();

    for (auto&& entityJson : entities)
    {
        auto entity = world.CreateEntity();
        for (auto&& [componentName, componentValue] : entityJson["components"].items())
        {
            if (componentName == "CameraComponent")
            {
                float fov = componentValue["fov"];
                float aspectRatio = componentValue["aspectRatio"];
                float nearZ = componentValue["nearZ"];
                float farZ = componentValue["farZ"];
                bool isOrtho = componentValue["isOrtho"];
                world.AddComponent(entity, CameraComponent{aspectRatio, nearZ, farZ, fov, isOrtho});
            }
            else if (componentName == "TransformComponent")
            {
                auto posJson = componentValue["position"];
                auto rotJson = componentValue["rotation"];
                auto scaleJson = componentValue["scale"];

                glm::vec3 position{posJson["x"], posJson["y"], posJson["z"]};
                glm::vec3 rotation{rotJson["x"], rotJson["y"], rotJson["z"]};
                float scale = scaleJson;

                world.AddComponent(entity, TransformComponent{position, rotation, scale});
            }
            else if (componentName == "NameComponent")
            {
                const std::string nameStr = componentValue["name"];
                world.AddComponent(entity, NameComponent{nameStr});
            }
            else if (componentName == "SpriteComponent")
            {
                const std::string shaderPath = componentValue["shaderPath"];
                auto sizeJson = componentValue["size"];
                glm::vec2 size{sizeJson["x"], sizeJson["y"]};

                const auto shader = engine::Application::Instance().GetResourceManager()->GetShader(shaderPath);

                const std::string texturePath = componentValue["texturePath"];
                auto sprite = std::make_unique<engine::Sprite>(texturePath, size);

                world.AddComponent(entity, SpriteComponent{std::move(sprite), shader});
            }
        }
    }
}

void SceneSerializerSystem::Save(std::string_view name)
{
    namespace fs = std::filesystem;
    fs::path filePath(name);

    std::ofstream ofs(name.data());
    using json = nlohmann::json;
    json j;

    using namespace engine::ecs;
    auto& world = engine::Application::Instance().GetWorld();

    for (const auto entity : Entities)
    {
        json entityJSON;
        if (world.HasComponent<DoNotSerializeComponent>(entity))
        {
            continue;
        }

        if (world.HasComponent<TransformComponent>(entity))
        {
            const auto& transformComponent = world.GetComponent<TransformComponent>(entity);
            json transCmp;

            // Position
            {
                transCmp["position"]["x"] = transformComponent.Position.x;
                transCmp["position"]["y"] = transformComponent.Position.y;
                transCmp["position"]["z"] = transformComponent.Position.z;
            }

            // Rotation
            {
                transCmp["rotation"]["x"] = transformComponent.Rotation.x;
                transCmp["rotation"]["y"] = transformComponent.Rotation.y;
                transCmp["rotation"]["z"] = transformComponent.Rotation.z;
            }

            transCmp["scale"] = transformComponent.UniformScale;

            entityJSON["components"]["TransformComponent"] = transCmp;
        }

        if (world.HasComponent<NameComponent>(entity))
        {
            const auto& nameComponent = world.GetComponent<NameComponent>(entity);
            json nameCmp;
            nameCmp["name"] = nameComponent.Name;

            entityJSON["components"]["NameComponent"] = nameCmp;
        }

        if (world.HasComponent<CameraComponent>(entity))
        {
            const auto& cameraComponent = world.GetComponent<CameraComponent>(entity);
            json cameraCmp;

            cameraCmp["fov"] = cameraComponent.Fov;
            cameraCmp["aspectRatio"] = cameraComponent.AspectRatio;
            cameraCmp["nearZ"] = cameraComponent.NearZ;
            cameraCmp["farZ"] = cameraComponent.FarZ;
            cameraCmp["isOrtho"] = cameraComponent.IsOrtho;

            entityJSON["components"]["CameraComponent"] = cameraCmp;
        }

        if (world.HasComponent<SpriteComponent>(entity))
        {
            const auto& spriteComponent = world.GetComponent<SpriteComponent>(entity);
            json spriteCmp;
            auto shader = spriteComponent.Shader.lock();
            spriteCmp["shaderPath"] = shader->GetName();
            spriteCmp["texturePath"] = spriteComponent.Sprite->GetTexturePath();
            auto size = spriteComponent.Sprite->GetSize();
            spriteCmp["size"]["x"] = size.x;
            spriteCmp["size"]["y"] = size.y;

            entityJSON["components"]["SpriteComponent"] = spriteCmp;
        }

        j["entities"].push_back(entityJSON);
    }

    ofs << j << std::endl;
    ofs.close();
}
