#include "pch.h"

#include "FrameRenderGraph.h"

#include "engine/Application.h"
#include "engine/ecs/systems/SpriteRendererSystem.h"

void FrameRenderGraph::Draw(float deltaTime)
{
    auto& world = engine::Application::Instance().GetWorld();
    const auto& spriteRenderer = world.GetSystem<engine::ecs::SpriteRendererSystem>();
    spriteRenderer->Draw(deltaTime);
}
