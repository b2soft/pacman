#pragma once
#include "ecs/systems/SceneSerializerSystem.h"
#include "engine/IGame.h"

struct FrameRenderGraph;
struct FrameUpdateGraph;

class PacmanGame : public engine::IGame
{
public:
    PacmanGame();
    ~PacmanGame() override;

    void OnUpdate(float deltaTime) override;
    void OnRender(float deltaTime) override;
    void OnStart() override;
    void OnFinish() override;

    void TestLoad();

private:
    std::unique_ptr<FrameRenderGraph> m_renderGraph;
    std::unique_ptr<FrameUpdateGraph> m_updateGraph;
    std::shared_ptr<SceneSerializerSystem> m_serializer;

    engine::ecs::Entity m_testCamera{engine::ecs::InvalidEntity};
    engine::ecs::Entity m_testSprite{engine::ecs::InvalidEntity};
};
