#pragma once

// OpenGL
#include <gl/glew.h>

// Math
#include <glm/glm.hpp>

// SDL
#include <SDL.h>

// STL
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>