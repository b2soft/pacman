#pragma once

namespace engine
{
class IGame
{
public:
    virtual ~IGame() = default;
    virtual void OnUpdate(float deltaTime) = 0;
    virtual void OnRender(float deltaTime) = 0;
    virtual void OnStart() = 0;
    virtual void OnFinish() = 0;
};
} // namespace engine