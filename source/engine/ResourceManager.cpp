#include "pch.h"

#include "ResourceManager.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "engine/render/Texture.h"

namespace engine
{
namespace
{
    const std::string k_resourcesFolder = R"(../../resources/)";
    const std::string k_shadersFolder = k_resourcesFolder + R"(shaders/)";
    const std::string k_texturesFolder = k_resourcesFolder + R"(textures/)";
} // namespace


using namespace std::literals;

std::weak_ptr<Texture> ResourceManager::GetTexture(std::string_view path)
{
    if (const auto it = m_textureCache.find(path); it != m_textureCache.end())
    {
        return it->second;
    }

    auto [it, _] = m_textureCache.emplace(path.data(), LoadTexture(k_texturesFolder + path.data()));
    return it->second;
}

std::weak_ptr<Shader> ResourceManager::GetShader(std::string_view path)
{
    if (const auto it = m_shaderCache.find(path); it != m_shaderCache.end())
    {
        return it->second;
    }

    auto [it, _] = m_shaderCache.emplace(path.data(), LoadShader(path));
    return it->second;
}

std::shared_ptr<Texture> ResourceManager::LoadTexture(std::string_view path) const
{
    int width = 0;
    int height = 0;
    int numChannels = 0;

    stbi_set_flip_vertically_on_load(true);
    auto* data = stbi_load(path.data(), &width, &height, &numChannels, 0);

    auto t = std::make_shared<Texture>(data, width, height, numChannels);
    stbi_image_free(data);

    return t;
}

std::shared_ptr<Shader> ResourceManager::LoadShader(std::string_view path) const
{
    auto readShader = [=](auto shaderPath) {
        std::ifstream shaderFile(shaderPath.data());

        std::stringstream shaderStream;
        shaderStream << shaderFile.rdbuf();
        shaderFile.close();

        return shaderStream.str();
    };

    const auto finalPath = k_shadersFolder + path.data();
    const auto vsCode = readShader(finalPath + ".vs"s);
    const char* vsCodePtr = vsCode.c_str();

    const auto fsCode = readShader(finalPath + ".fs"s);
    const char* fsCodePtr = fsCode.c_str();

    return std::make_shared<Shader>(vsCodePtr, fsCodePtr, path.data());
}
} // namespace engine