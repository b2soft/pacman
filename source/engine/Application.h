#pragma once

#include "HighResolutionClock.h"
#include "IGame.h"
#include "Singleton.h"
#include "ecs/SystemsHolder.h"

struct SDL_Window;
struct SDL_Surface;

namespace engine
{
class ResourceManager;

enum class State
{
    Work,
    Exit
};

namespace ecs
{
    class World;
}

class Application : public Singleton<Application>
{
public:
    explicit Application(token);
    Application& operator=(const Application&) = delete;
    Application(const Application&) = delete;
    Application& operator=(Application&&) = delete;
    Application(Application&&) = delete;
    ~Application();

    static void Run(IGame* game);
    [[nodiscard]] ResourceManager* GetResourceManager() const;
    [[nodiscard]] ecs::World& GetWorld() const;
    [[nodiscard]] const HighResolutionClock* GetClock() const;

    static glm::vec2 GetViewportSize();

private:
    void MainLoop();
    void ProcessEvents();

    void Init();
    void Update(float deltaTime) const;
    void Draw(float deltaTime) const;

    SDL_Window* m_window{nullptr};
    State m_state{State::Work};

    std::unique_ptr<ResourceManager> m_resourceManager;
    std::unique_ptr<ecs::World> m_ecs;

    uint64_t m_frameCount{0};

    HighResolutionClock m_updateClock;
    IGame* m_game{nullptr};

    SystemsHolder m_engineSystems;
};
} // namespace engine
