#pragma once

namespace engine
{

class Texture;
class Shader;

class Sprite
{
public:
    Sprite(std::string_view texturePath, glm::vec2 size);

    ~Sprite();
    Sprite(Sprite&&) = default;
    Sprite& operator=(Sprite&&) = default;
    Sprite(const Sprite&) = delete;
    Sprite& operator=(const Sprite&) = delete;

    void Draw() const;

    [[nodiscard]] std::string_view GetTexturePath() const;
    [[nodiscard]] glm::vec2 GetSize() const;

private:
    glm::vec2 m_size{0.f};

    GLuint m_vboId{0};
    GLuint m_vaoId{0};

    std::weak_ptr<Texture> m_texture;

    std::string m_texturePath;
};

} // namespace engine
