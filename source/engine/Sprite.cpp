#include "pch.h"

#include "Sprite.h"

#include "Application.h"
#include "Errors.h"
#include "ResourceManager.h"
#include "render/Texture.h"
#include "render/Vertex.h"

namespace engine
{

Sprite::Sprite(std::string_view texturePath, glm::vec2 size)
    : m_size(size)
{
    if (m_vboId == 0)
    {
        glGenBuffers(1, &m_vboId);
    }

    if (m_vaoId == 0)
    {
        glGenVertexArrays(1, &m_vaoId);
    }

    glm::vec2 offset = size * 0.5f;

    Vertex vertexData[6];

    // Triangle 1
    vertexData[0].SetPosition({offset.x, offset.y});
    vertexData[0].SetUV({1.f, 1.f});

    vertexData[1].SetPosition({-offset.x, offset.y});
    vertexData[1].SetUV({0.f, 1.f});

    vertexData[2].SetPosition({- offset.x, -offset.y});
    vertexData[2].SetUV({0.f, 0.f});

    // Ttiangle 2
    vertexData[3].SetPosition({-offset.x, -offset.y});
    vertexData[3].SetUV({0.f, 0.f});

    vertexData[4].SetPosition({offset.x, -offset.y});
    vertexData[4].SetUV({1.f, 0.0f});

    vertexData[5].SetPosition({offset.x, offset.y});
    vertexData[5].SetUV({1.f, 1.f});

    glBindBuffer(GL_ARRAY_BUFFER, m_vboId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(m_vaoId);

    m_texture = Application::Instance().GetResourceManager()->GetTexture(texturePath);

    m_texturePath = texturePath;
}

Sprite::~Sprite()
{
    if (m_vboId != 0)
    {
        glDeleteBuffers(1, &m_vboId);
        m_vboId = 0;
    }
}

void Sprite::Draw() const
{
    glBindBuffer(GL_ARRAY_BUFFER, m_vboId);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), nullptr);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, UV)));

    if (const auto texture = m_texture.lock())
    {
        texture->Bind();
    }
    else
    {
        FatalError("Texture is missing!");
    }

    glDrawArrays(GL_TRIANGLES, 0, 6);
}

std::string_view Sprite::GetTexturePath() const
{
    return m_texturePath;
}

glm::vec2 Sprite::GetSize() const
{
    return m_size;
}
} // namespace engine