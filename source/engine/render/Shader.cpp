#include "pch.h"

#include "Shader.h"

#include "engine/Errors.h"

using namespace std::literals;

namespace
{
constexpr std::size_t k_infoLogSize = 512;
}

namespace engine
{

Shader::Shader(const char* vsCodePtr, const char* fsCodePtr, std::string_view name)
{
    m_vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
    if (m_vertexShaderId == 0)
    {
        FatalError("Failed to create Vertex Shader");
    }
    CompileShader(vsCodePtr, m_vertexShaderId);

    m_fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);

    if (m_fragmentShaderId == 0)
    {
        FatalError("Failed to create Fragment Shader");
    }
    CompileShader(fsCodePtr, m_fragmentShaderId);

    Link();

    m_name = name;
}

void Shader::Link()
{
    m_programId = glCreateProgram();

    glAttachShader(m_programId, m_vertexShaderId);
    glAttachShader(m_programId, m_fragmentShaderId);

    glLinkProgram(m_programId);

    GLint linkStatus = 0;
    glGetProgramiv(m_programId, GL_LINK_STATUS, &linkStatus);
    if (linkStatus == GL_FALSE)
    {
        std::string errorLog;
        errorLog.reserve(k_infoLogSize);
        glGetProgramInfoLog(m_programId, k_infoLogSize, nullptr, &errorLog[0]);

        glDeleteProgram(m_programId);

        std::cout << errorLog.data() << std::endl;
        FatalError("Failed to Link a Program");
        return;
    }

    glDeleteShader(m_vertexShaderId);
    glDeleteShader(m_fragmentShaderId);
}

void Shader::Bind() const
{
    glUseProgram(m_programId);
}

void Shader::SetFloat(std::string_view uniformName, float value) const
{
    glUniform1f(glGetUniformLocation(m_programId, uniformName.data()), value);
}

void Shader::SetInt(std::string_view uniformName, int value) const
{
    glUniform1i(glGetUniformLocation(m_programId, uniformName.data()), value);
}

void Shader::SetBool(std::string_view uniformName, bool value) const
{
    glUniform1i(glGetUniformLocation(m_programId, uniformName.data()), static_cast<bool>(value));
}

void Shader::SetMatrix(std::string_view uniformName, const glm::mat4& value) const
{
    glUniformMatrix4fv(glGetUniformLocation(m_programId, uniformName.data()), 1, GL_FALSE, &value[0][0]);
}

std::string_view Shader::GetName() const
{
    return m_name;
}

void Shader::CompileShader(const char* code, GLuint id)
{
    glShaderSource(id, 1, &code, nullptr);
    glCompileShader(id);
    GLint success = 0;
    glGetShaderiv(id, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        std::string infoLog;
        infoLog.resize(k_infoLogSize);
        glGetShaderInfoLog(id, k_infoLogSize, nullptr, &infoLog[0]);
        std::cout << "Failed to compile shader" << std::endl;
        FatalError(infoLog);
    }
}

} // namespace engine
