#pragma once

namespace engine
{
class Texture
{
public:
    Texture(const unsigned char* data, int width, int height, bool generateMips = true);

    ~Texture() = default;
    Texture(const Texture&) = delete;
    Texture& operator=(const Texture&) = delete;
    Texture(Texture&&) = default;
    Texture& operator=(Texture&&) = default;

    void Bind() const;

private:
    int m_width{0};
    int m_height{0};
    GLuint m_textureId{0};
};
} // namespace engine