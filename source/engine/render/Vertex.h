#pragma once

struct Vertex
{
    struct Position
    {
        float X;
        float Y;
    } Position;

    struct UV
    {
        GLfloat U;
        GLfloat V;
    } UV;

    void SetPosition(glm::vec2 position)
    {
        Position.X = position.x;
        Position.Y = position.y;
    }

    void SetUV(glm::vec2 uv)
    {
        UV.U = uv.x;
        UV.V = uv.y;
    }
};