#pragma once

namespace engine
{

class Shader
{
public:
    Shader(const char* vsCodePtr, const char* fsCodePtr, std::string_view name);
    void Bind() const;

    void SetFloat(std::string_view uniformName, float value) const;
    void SetInt(std::string_view uniformName, int value) const;
    void SetBool(std::string_view uniformName, bool value) const;
    void SetMatrix(std::string_view uniformName, const glm::mat4& matrix) const;
    std::string_view GetName() const;

private:
    static void CompileShader(const char* code, GLuint id);

    void Link();

    GLuint m_programId{0};
    GLuint m_vertexShaderId{0};
    GLuint m_fragmentShaderId{0};

    std::string m_name;
};

} // namespace engine