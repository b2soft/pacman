#include "pch.h"

#include "Application.h"

#include "Errors.h"
#include "ecs/ECS.h"
#include "ecs/components/CameraComponent.h"
#include "ecs/components/NameComponent.h"
#include "ecs/components/TransformComponent.h"
#include "engine/ResourceManager.h"

namespace
{
constexpr int k_screenWidth = 1280;
constexpr int k_screenHeight = 720;
} // namespace

namespace engine
{

Application::Application(token)
{
}
Application::~Application() = default;

void Application::Init()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        std::cerr << "SDL init error : " << SDL_GetError() << std::endl;
    }

    m_window = SDL_CreateWindow("PacMan", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, k_screenWidth, k_screenHeight,
                                SDL_WINDOW_OPENGL);
    if (!m_window)
    {
        FatalError("Window can not be created!");
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    const auto* glContext = SDL_GL_CreateContext(m_window);
    if (!glContext)
    {
        FatalError("SDL_GLContext can not be created!");
    }

    const GLenum error = glewInit();
    if (error != GLEW_OK)
    {
        FatalError("GLEW can not be initialized!");
    }

    SDL_GL_SetSwapInterval(0);

    glClearColor(0.0f, 1.0f, 1.0f, 1.0f);

    m_resourceManager = std::make_unique<ResourceManager>();

    m_ecs = std::make_unique<ecs::World>();
    m_ecs->Init();

    m_engineSystems.Register();

    m_game->OnStart();
}

void Application::Update(float deltaTime) const
{
    m_game->OnUpdate(deltaTime);
}

void Application::ProcessEvents()
{
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_QUIT:
            m_state = State::Exit;
            break;

            // case SDL_MOUSEMOTION:
            //	//TODO: Mouse Motion event
            //	std::cout << event.motion.x << " " << event.motion.y << std::endl;
            //	break;
        default:
            break;
        }
    }
}

void Application::Draw(float deltaTime) const
{
    glViewport(0, 0, k_screenWidth, k_screenHeight);
    glClearDepth(1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_game->OnRender(deltaTime);

    SDL_GL_SwapWindow(m_window);
}

void Application::MainLoop()
{
    while (m_state != State::Exit)
    {
        ProcessEvents();

        ++m_frameCount;
        m_updateClock.Tick();
        const auto dt = static_cast<float>(m_updateClock.GetDeltaSeconds());
        Update(dt);
        Draw(dt);
    }

    m_game->OnFinish();
}

void Application::Run(IGame* game)
{
    Instance().m_game = game;
    Instance().Init();
    Instance().MainLoop();
}

ResourceManager* Application::GetResourceManager() const
{
    return m_resourceManager.get();
}

ecs::World& Application::GetWorld() const
{
    return *m_ecs;
}

const HighResolutionClock* Application::GetClock() const
{
    return &m_updateClock;
}

glm::vec2 Application::GetViewportSize()
{
    // TODO: @b2soft return real size
    return {k_screenWidth, k_screenHeight};
}
} // namespace engine