#include "pch.h"

#include "CameraSystem.h"

#include <glm/gtc/matrix_transform.hpp>

#include "engine/Application.h"

#include <engine/ecs/components/CameraComponent.h>
#include <engine/ecs/components/TransformComponent.h>

namespace engine
{
namespace ecs
{
    // TODO: @b2soft maybe caching calculation results
    Entity CameraSystem::GetActiveCamera() const
    {
        return m_activeCamera;
    }

    void CameraSystem::SetActiveCamera(Entity cameraEntity)
    {
        m_activeCamera = cameraEntity;
    }

    glm::mat4 CameraSystem::GetViewMatrix(Entity cameraEntity) const
    {
        auto& world = Application::Instance().GetWorld();
        const auto& transformComponent = world.GetComponent<TransformComponent>(cameraEntity);

        glm::mat4 outMat{1.0f};
        outMat = glm::translate(outMat, -transformComponent.Position);
        // TODO: @b2soft implement rotation
        return outMat;
    }

    glm::mat4 CameraSystem::GetInvViewMatrix(Entity cameraEntity) const
    {
        const auto viewMatrix = GetViewMatrix(cameraEntity);
        return glm::inverse(viewMatrix);
    }

    glm::mat4 CameraSystem::GetProjMatrix(Entity cameraEntity) const
    {
        auto& world = Application::Instance().GetWorld();
        const auto& cameraComponent = world.GetComponent<CameraComponent>(cameraEntity);

        if (cameraComponent.IsOrtho)
        {
            const auto viewportSize = Application::GetViewportSize() * 0.5f;
            return glm::ortho(-viewportSize.x, viewportSize.x, -viewportSize.y, viewportSize.y);
        }

        return glm::perspective(cameraComponent.Fov, cameraComponent.AspectRatio, cameraComponent.NearZ,
                                cameraComponent.FarZ);
    }

    glm::mat4 CameraSystem::GetInvProjMatrix(Entity cameraEntity) const
    {
        const auto projMatrix = GetProjMatrix(cameraEntity);
        return glm::inverse(projMatrix);
    }
} // namespace ecs
} // namespace engine