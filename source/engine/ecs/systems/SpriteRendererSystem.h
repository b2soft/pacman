#pragma once
#include "engine/ecs/ECS.h"

namespace engine
{
namespace ecs
{
    class SpriteRendererSystem : public System
    {
    public:
        void Draw(float deltaTime) const;
    };
} // namespace ecs

} // namespace engine