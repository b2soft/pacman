#include "pch.h"

#include "SpriteRendererSystem.h"

#include "engine/Application.h"
#include "engine/ResourceManager.h"
#include "engine/Sprite.h"
#include "engine/ecs/components/CameraComponent.h"
#include "engine/ecs/components/SpriteComponent.h"
#include "engine/ecs/components/TransformComponent.h"
#include "engine/ecs/systems/CameraSystem.h"
#include "engine/render/Shader.h"

namespace engine
{
namespace ecs
{
    void SpriteRendererSystem::Draw(float deltaTime) const
    {
        auto& world = Application::Instance().GetWorld();
        const auto cameraSystem = world.GetSystem<CameraSystem>();
        auto activeCamera = cameraSystem->GetActiveCamera();

        const auto viewMatrix = cameraSystem->GetViewMatrix(activeCamera);
        const auto projMatrix = cameraSystem->GetProjMatrix(activeCamera);

        // m_camera->Update();

        auto* clock = Application::Instance().GetClock();

        for (const auto entity : Entities)
        {
            auto& spriteCmp = world.GetComponent<SpriteComponent>(entity);
            const auto shader = spriteCmp.Shader.lock();
            shader->Bind();
            shader->SetFloat("time", static_cast<float>(clock->GetTotalSeconds()));
            shader->SetMatrix("viewProjMatrix", projMatrix * viewMatrix);

            spriteCmp.Sprite->Draw();
        }
    }

} // namespace ecs
} // namespace engine
