#pragma once

#include "engine/ecs/ECS.h"

namespace engine
{
namespace ecs
{
    class CameraSystem : public ecs::System
    {
    public:
        Entity GetActiveCamera() const;
        void SetActiveCamera(Entity cameraEntity);

        glm::mat4 GetViewMatrix(Entity cameraEntity) const;
        glm::mat4 GetInvViewMatrix(Entity cameraEntity) const;

        glm::mat4 GetProjMatrix(Entity cameraEntity) const;
        glm::mat4 GetInvProjMatrix(Entity cameraEntity) const;

    private:
        Entity m_activeCamera{InvalidEntity};
    };
} // namespace ecs
} // namespace engine
