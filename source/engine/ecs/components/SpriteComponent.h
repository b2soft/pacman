#pragma once

#include "engine/Sprite.h"

namespace engine
{
class Shader;

namespace ecs
{
    struct SpriteComponent
    {
        std::unique_ptr<Sprite> Sprite;
        std::weak_ptr<Shader> Shader;
    };
} // namespace ecs
} // namespace engine