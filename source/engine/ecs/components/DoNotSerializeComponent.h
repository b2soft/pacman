#pragma once

namespace engine
{
namespace ecs
{
    struct DoNotSerializeComponent
    {
    };
} // namespace ecs
} // namespace engine