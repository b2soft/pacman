#pragma once

#include <string>

namespace engine
{
namespace ecs
{
    struct NameComponent
    {
        std::string Name;
    };
} // namespace ecs
} // namespace engine