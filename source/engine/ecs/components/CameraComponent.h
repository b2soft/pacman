#pragma once

namespace engine
{
namespace ecs
{
    struct CameraComponent
    {
        float AspectRatio{1.0f};
        float NearZ{0.01f};
        float FarZ{100.0f};
        float Fov{45.0f};
        bool IsOrtho{false};
    };
} // namespace ecs
} // namespace engine