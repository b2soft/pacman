#pragma once

namespace engine
{
namespace ecs
{
    struct TransformComponent
    {
        glm::vec3 Position{0.f};
        glm::vec3 Rotation{0.f};
        float UniformScale{1.f};
    };
} // namespace ecs
} // namespace engine