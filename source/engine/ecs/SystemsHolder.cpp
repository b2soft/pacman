#include "pch.h"

#include "SystemsHolder.h"

#include "ECS.h"

#include "engine/Application.h"

#include "components/CameraComponent.h"
#include "components/DoNotSerializeComponent.h"
#include "components/NameComponent.h"
#include "components/SpriteComponent.h"
#include "components/TransformComponent.h"

#include "systems/CameraSystem.h"
#include "systems/SpriteRendererSystem.h"

namespace engine
{
void SystemsHolder::Register()
{
    auto& ecsWorld = Application::Instance().GetWorld();

    // Register components
    ecsWorld.RegisterComponent<ecs::NameComponent>();
    ecsWorld.RegisterComponent<ecs::CameraComponent>();
    ecsWorld.RegisterComponent<ecs::TransformComponent>();
    ecsWorld.RegisterComponent<ecs::SpriteComponent>();
    ecsWorld.RegisterComponent<ecs::DoNotSerializeComponent>();

    // Register systems
    // Camera System
    {
        m_cameraSystem = ecsWorld.RegisterSystem<ecs::CameraSystem>();

        ecs::ComponentMask mask;
        mask.set(ecsWorld.GetComponentType<ecs::CameraComponent>());
        mask.set(ecsWorld.GetComponentType<ecs::TransformComponent>());
        ecsWorld.SetSystemComponentMask<ecs::CameraSystem>(mask);
    }

    // Sprite Renderer System
    {
        m_spriteRendererSystem = ecsWorld.RegisterSystem<ecs::SpriteRendererSystem>();

        ecs::ComponentMask mask;
        mask.set(ecsWorld.GetComponentType<ecs::SpriteComponent>());
        mask.set(ecsWorld.GetComponentType<ecs::TransformComponent>());
        ecsWorld.SetSystemComponentMask<ecs::SpriteRendererSystem>(mask);
    }
}
} // namespace engine
