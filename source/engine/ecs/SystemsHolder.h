#pragma once

namespace engine
{
namespace ecs
{
    class CameraSystem;
    class SpriteRendererSystem;
} // namespace ecs

class SystemsHolder
{
public:
    virtual void Register();

private:
    std::shared_ptr<ecs::CameraSystem> m_cameraSystem;
    std::shared_ptr<ecs::SpriteRendererSystem> m_spriteRendererSystem;
};

} // namespace engine
