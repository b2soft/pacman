#pragma once

#include <memory>

namespace engine
{

template <typename T>
class Singleton
{
public:
    static T& Instance();

    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

protected:
    struct token
    {
    };
    Singleton() = default;
};

template <typename T>
T& Singleton<T>::Instance()
{
    static const std::unique_ptr<T> instance = std::make_unique<T>(token{});
    return *instance;
}
} // namespace engine