#pragma once

#include <chrono>

namespace engine
{
class HighResolutionClock
{
public:
    HighResolutionClock();

    // Tick the high resolution clock.
    // Tick the clock before reading the delta time for the first time.
    // Only tick the clock once per frame.
    // Use the Get* functions to return the elapsed time between ticks.
    void Tick();

    // Reset the clock
    void Reset();

    [[nodiscard]] double GetDeltaNanoseconds() const;
    [[nodiscard]] double GetDeltaMicroseconds() const;
    [[nodiscard]] double GetDeltaMilliseconds() const;
    [[nodiscard]] double GetDeltaSeconds() const;

    [[nodiscard]] double GetTotalNanoseconds() const;
    [[nodiscard]] double GetTotalMicroseconds() const;
    [[nodiscard]] double GetTotalMilliseconds() const;
    [[nodiscard]] double GetTotalSeconds() const;

private:
    // Initial time point
    std::chrono::high_resolution_clock::time_point m_t0;

    // Time since last tick
    std::chrono::high_resolution_clock::duration m_deltaTime;
    std::chrono::high_resolution_clock::duration m_totalTime;
};

} // namespace engine