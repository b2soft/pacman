#include "pch.h"

#include "Errors.h"

namespace engine
{
void FatalError(std::string_view error)
{
    std::cerr << error << std::endl;
    SDL_Quit();
    // exit(1);
}
} // namespace engine
