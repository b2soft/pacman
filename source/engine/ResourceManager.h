#pragma once
#include <memory>

#include "render/Shader.h"

namespace engine
{

class Texture;

class ResourceManager
{
public:
    std::weak_ptr<Texture> GetTexture(std::string_view name);
    std::weak_ptr<Shader> GetShader(std::string_view name);

    struct string_hash
    {
        using hash_type = std::hash<std::string_view>;
        using is_transparent = void;

        size_t operator()(const char* str) const
        {
            return hash_type{}(str);
        }
        size_t operator()(std::string_view str) const
        {
            return hash_type{}(str);
        }
        size_t operator()(std::string const& str) const
        {
            return hash_type{}(str);
        }
    };

private:
    template <class T>
    using Cache = std::unordered_map<std::string, std::shared_ptr<T>, string_hash, std::equal_to<>>;

    [[nodiscard]] std::shared_ptr<Texture> LoadTexture(std::string_view path) const;
    [[nodiscard]] std::shared_ptr<Shader> LoadShader(std::string_view path) const;

    Cache<Texture> m_textureCache;
    Cache<Shader> m_shaderCache;
};

} // namespace engine