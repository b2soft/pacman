#include "engine/Application.h"
#include "game/PacmanGame.h"

int main(int, char**)
{
    PacmanGame game;
    engine::Application::Run(&game);
    return 0;
}
