#version 460

in vec2 uv;

out vec4 FragColor;

uniform float time;
uniform sampler2D testSampler;

void main()
{
    vec4 texColor = texture(testSampler, uv);
    texColor.r *= (cos(time * 4.0) + 1.0) * 0.5;
    texColor.g *= (cos(time * 8.0) + 1.0) * 0.5;
    texColor.b *= (sin(time) + 1.0) * 0.5;

    FragColor = texColor;
}