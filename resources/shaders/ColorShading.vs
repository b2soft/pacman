#version 460

layout(location = 0) in vec2 aPos;
layout(location = 1) in vec2 aUV;

out vec2 uv;

uniform mat4 viewProjMatrix;

void main()
{
    gl_Position.xy = (viewProjMatrix * vec4(aPos, 0.0, 1.0)).xy;
    gl_Position.z = 0.0;
    gl_Position.w = 1.0;
    uv = aUV;
}