# PACMAN #

# Pacman (To be renamed)
## _Pacman inspired 2D game_

## How to build a project for Visual Studio

1. Download this repository
2. Install vcpkg from [here] (https://vcpkg.io/en/getting-started.html)
3. Add `VCPKG_ROOT` pointing to vcpkg install folder to system env vars
4. Run `make_vs_project.bat`
5. Open `./project/Pacman.sln`
6. Choose needed config (Debug or Release) and build
7. Resulting binaries are in `./bin/` subfolders