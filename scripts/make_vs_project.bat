@echo off
call get_packages_win-x64.bat
cd ..
rmdir project /s /q
mkdir project
cd project
cmake ../source -DCMAKE_TOOLCHAIN_FILE=%VCPKG_ROOT%/scripts/buildsystems/vcpkg.cmake
pause.